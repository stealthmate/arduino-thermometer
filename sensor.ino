#include "DHT.h"

int DHTPIN = 8;
DHT dht(DHTPIN, DHT11);

unsigned long int last_reading = 2100;
float last_temp = 0.0;
float last_humid = 0.0;
int last_read = -1; // -1 = init, 0 - temp, 1 - humid
const unsigned long int DELAY = 2000;

void sensor_init() {
  pinMode(DHTPIN, INPUT);
  dht.begin();
}

float sensor_readtemp() {
#ifdef DEBUG
  return 40 * pot_read();
#endif
  if((last_read == 1) || (millis() - last_reading > DELAY)) {
    last_temp = dht.readTemperature();
    last_reading = millis();
    last_read = 0;
  }
  return last_temp;
}

float sensor_readhumid() {
#ifdef DEBUG
  return (95 - 15) * pot_read() + 15.0;
#endif
  if((last_read == 0) || (millis() - last_reading > DELAY)) {
    last_humid = dht.readHumidity();
    last_reading = millis();
    last_read = 1;
  }
  return last_humid;
}
