#include <math.h>

int RED_PIN = 11;
int GREEN_PIN = 10;
int BLUE_PIN = 9;

void hsv2rgb(float *hsv, float *rgb) {
  float h = hsv[0];
  float s = hsv[1];
  float v = hsv[2];

  float c = s * v;
  float hp = h / 60;
  float x = hp - floor(hp / 2) * 2;
  x = 1 - fabs(x - 1);
  x = c * x;

  rgb[0] = 0;
  rgb[1] = 0;
  rgb[2] = 0;

  if (0 <= hp && hp < 1) {
    rgb[0] = c;
    rgb[1] = x;
  } else if (1 <= hp && hp < 2) {
    rgb[0] = x;
    rgb[1] = c;
  } else if (2 <= hp && hp < 3) {
    rgb[1] = c;
    rgb[2] = x;
  } else if (3 <= hp && hp < 4) {
    rgb[1] = x;
    rgb[2] = c;
  } else if (4 <= hp && hp < 5) {
    rgb[0] = x;
    rgb[2] = c;
  } else if (5 <= hp && hp < 6) {
    rgb[0] = c;
    rgb[2] = x;
  }

  float m = (v - c);
  rgb[0] += m;
  rgb[1] += m;
  rgb[2] += m;
}

void color_init() {
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);
}

void rgb2led(float *rgb, int *led) {

  for (int i = 0; i < 3; i++) {
    rgb[i] = log(rgb[i] * (M_E - 1) + 1);
  }

  led[0] = 255 - (100 * rgb[0]);
  led[1] = 255 - 170 * rgb[1];
  led[2] = 255 - 255 * rgb[2];
}

void color_setHSV(float *hsv) {
  float rgb[] = {0, 0, 0};
  hsv2rgb(hsv, rgb);
  int led[] = {0, 0, 0};
  rgb2led(rgb, led);

  analogWrite(RED_PIN, led[0]);
  analogWrite(GREEN_PIN, led[1]);
  analogWrite(BLUE_PIN, led[2]);
}
