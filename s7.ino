int S7_SWEEP_DELAY = 4;

int S7_SEGMENT_PINS[] = {7, 6, 5, 4, 3, 2, 1, 0};
int S7_DIGIT_PINS[] = {14, 15, 16, 17};
int S7_DIGITS[] = {
  B11111100,
  B01100000,
  B11011010,
  B11110010,
  B01100110,
  B10110110,
  B10111110,
  B11100000,
  B11111110,
  B11110110
};
int S7_H = B01101110;
int S7_i = B00001100;
int S7_L = B00011100;
int S7_o = B00111010;
int S7_DOT = B00000001;
int S7_NULL = B00000000;

int S7_BUFFER[4] = {S7_NULL, S7_NULL, S7_NULL, S7_NULL};

void s7_init() {
  for(int i=0;i<8;i++){ 
    pinMode(S7_SEGMENT_PINS[i], OUTPUT);
  }
  for(int i=0;i<4;i++) {
    pinMode(S7_DIGIT_PINS[i], OUTPUT);
  }
}

void s7_write_segments(int d) {
  for(int i=0;i < 8;i++) {
    if(d & (1 << i)) {
      digitalWrite(S7_SEGMENT_PINS[7 - i], HIGH);
    } else {
      digitalWrite(S7_SEGMENT_PINS[7 - i], LOW);
    }
  }
}

void s7_sweep() {
  for(int i=0;i<4;i++) {
    digitalWrite(S7_DIGIT_PINS[i], LOW);
    s7_write_segments(S7_BUFFER[i]);
    delay(S7_SWEEP_DELAY);
    digitalWrite(S7_DIGIT_PINS[i], HIGH);
  }
}

void s7_set_single(int i, int d) {
  S7_BUFFER[i] = d;
}

void s7_set_hi() {
  s7_set_single(0, S7_H);
  s7_set_single(1, S7_i);
  s7_set_single(2, S7_NULL);
  s7_set_single(3, S7_NULL);
}

void s7_set_lo() {
  s7_set_single(0, S7_L);
  s7_set_single(1, S7_o);
  s7_set_single(2, S7_NULL);
  s7_set_single(3, S7_NULL);
}

void s7_set_float(float f) {
  int whole = int(f);
  int dec = int(f * 100) % 100;

  if(whole >= 100) {
    s7_set_hi();
  } else if(whole < 0) {
    s7_set_lo();
  } else {
    s7_set_single(0, whole >= 10 ? S7_DIGITS[(whole / 10) % 10] : S7_NULL);
    s7_set_single(1, S7_DIGITS[whole % 10] | S7_DOT);
    s7_set_single(2, S7_DIGITS[(dec / 10) % 10]);
    s7_set_single(3, S7_DIGITS[dec % 10]);
  }
}
