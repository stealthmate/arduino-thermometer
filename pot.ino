const int POT_PIN = 18;

const double MIN_V = 0;
const double A = 1 - MIN_V;
const double B = log(1 + A);

void pot_init() {
  pinMode(POT_PIN, INPUT);
}

float pot_read() {
  double P = analogRead(POT_PIN) / 1024.0;

  double v = exp(P * B) - A;
  return v;
}
