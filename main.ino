extern const int SWITCH_LOW;
extern const int SWITCH_HIGH;

//#define DEBUG

enum State {
  TEMP,
  HUMID
};
const int N_STATES = 2;
const State STATES[] = {
  TEMP,
  HUMID
};
int current_state = 0;

void setup() {
  for (int i = 0; i < 20; i++) {
    pinMode(i, INPUT);
    digitalWrite(i, LOW);
  }

  s7_init();
  sensor_init();
  switch_init();
  color_init();
  pot_init();
#ifdef DEBUG
  Serial.begin(9600);
#endif
}

void set_state(int state) {
  current_state = state;
}

void proc_showtemp() {
  float temp = sensor_readtemp();
  s7_set_float(temp);
  s7_sweep();
#ifdef DEBUG
  float b = 1.;
#else
  float b = pot_read();
#endif
  float hsv[] = {temp_to_hue(temp), 1., b};
  color_setHSV(hsv);
}

void proc_showhumid() {
  float humid = sensor_readhumid();
  s7_set_float(humid);
  s7_sweep();
#ifdef DEBUG
  float b = 1.;
#else
  float b = pot_read();
#endif
  float hsv[] = {humid_to_hue(humid), 1., b};
  color_setHSV(hsv);
}

void proc() {
  switch (current_state) {
    case TEMP: proc_showtemp(); break;
    case HUMID: proc_showhumid(); break;
    default: break;
  }
}


int RANGE = 60;
int RANGE_R = 15;
int RANGE_B = 120;
int RANGE_G = 20;

float X = 0.0;
float dx = 1.;

void loop() {
  int switch_state = switch_read();
  if (switch_state == SWITCH_HIGH) set_state(TEMP);
  else set_state(HUMID);

  proc();
}
