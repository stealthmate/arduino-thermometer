const float LOW_HUE_TEMP = 240.0;
const float HIGH_HUE_TEMP = 10.0;

const float LOW_HUE_HUMID = 10.0;
const float HIGH_HUE_HUMID = 240.0;


float temp_to_hue(float temp) {
  float v = 260 - 7*temp;
  if(v > LOW_HUE_TEMP) v = LOW_HUE_TEMP;
  if(v < HIGH_HUE_TEMP) v = HIGH_HUE_TEMP;
  return v;
}

float humid_to_hue(float humid) {
  float v = (humid - 20) * 3.7;
  if(v > HIGH_HUE_HUMID) v = HIGH_HUE_HUMID;
  if(v < LOW_HUE_HUMID) v = LOW_HUE_HUMID;
  return v;
}
