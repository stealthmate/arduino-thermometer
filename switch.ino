const int SWITCH_PIN = 12;

extern const int SWITCH_LOW = 0;
extern const int SWITCH_HIGH = 1;

void switch_init() {
  pinMode(SWITCH_PIN, INPUT);
}

int switch_read() {
  return digitalRead(SWITCH_PIN);
}
